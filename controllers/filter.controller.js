const FilterService = require('../services/filter.service');

exports.getFilters = async (req, res, next) => {
    try {
        const filters = await FilterService.getFilters();
        return res.status(200).json({ status: 200, data: filters });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.createFilter = async (req, res, next) => {
    try {
        const filter = await FilterService.createFilter(req.body.title);
        return res.status(200).json({ status: 200, data: filter });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeFilters = async (req, res, next) => {
    try {
        const resp = FilterService.removeFilters();
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}


exports.getFilter = async (req, res, next) => {
    try {
        const filter = await FilterService.getFilter(req.params.filterId);
        return res.status(200).json({ status: 200, data: filter });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.updateFilter = async (req, res, next) => {
    try {
        await FilterService.updateFilter(req.params.filterId, req.body.title);
        const filter = await FilterService.getFilter(req.params.filterId);
        return res.status(200).json({ status: 200, data: filter });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeFilter = async (req, res, next) => {
    try {
        const resp = await FilterService.removeFilter(req.params.filterId);
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.addValueToFilter = async (req, res, next)  => {
    try {
        await FilterService.addValueToFilter(req.params.filterId, req.params.valueId);
        const filter = FilterService.getFilter(req.params.filterId);
        return res.status(200).json({ status: 200, data: filter });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeValueFromFilter = async (req, res, next) => {
    try {
        await FilterService.removeValueFromFilter(req.params.filterId, req.params.valueId);
        const filter = FilterService.getFilter(req.params.filterId);
        return res.status(200).json({ status: 200, data: filter });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}