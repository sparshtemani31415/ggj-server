const ProductService = require('../services/product.service');

exports.getProducts = async (req, res, next) => {
    try {
        const products = await ProductService.getProducts(req.query.page, req.qeury.limit);
        return res.status(200).json({ status: 200, data: products });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.createProduct = async (req, res, next) => {
    try {
        const product = await ProductService.createProduct(req.body, req.file.filename);
        return res.status(200).json({ status: 200, data: product });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeProducts = async (req, res, next) => {
    try {
        const resp = await ProductService.removeProducts();
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}


exports.addImage = async (req, res, next) => {
    try {
        if (req.body.isMain) {
            await ProductService.updateMainImage(req.file.path);
            const product = await ProductService.getProduct(req.params.productId);
            return res.status(200).json({ status: 200, data: product });
        }
        else {
            await ProductService.addImage(req.file.path);
            const product = await ProductService.getProduct(req.params.productId);
            return res.status(200).json({ status: 200, data: product });
        }
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeImage = async (req, res, next) => {
    try {
        await ProductService.removeImage(req.params.productId, req.body.image);
        const product = await ProductService.getProduct(req.params.productId);
        return res.status(200).json({ status: 200, data: product });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.getProduct = async (req, res, next) => {
    try {
        const product = await ProductService.getProduct(req.params.productId);
        return res.status(200).json({ status: 200, data: product });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.updateProduct = async (req, res, next) => {
    try {
        await ProductService.updateProduct(req.body);
        const product = await ProductService.getProduct(req.params.productId);
        return res.status(200).json({ status: 200, data: product });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeProduct = async (req, res, next) => {
    try {
        const resp = await ProductService.removeProduct(req.params.productId);
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.addValueToProduct = async (req, res, next) => {
    try {
        await ProductService.addValueToProduct(req.params.productId, req.params.valueId);
        const product = await ProductService.getProduct(req.params.productId);
        return res.status(200).json({ status: 200, data: product });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeValueFromProduct = async (req, res, next) => {
    try {
        await ProductService.removeValueFromProduct(req.params.productId, req.params.valueId);
        const product = await ProductService.getProduct(req.params.productId);
        return res.status(200).json({ status: 200, data: product });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.getProductsWithCategory = async (req, res, next) => {
    try {
        const products = await ProductService.getProductsInCategory(req.params.categoryId, req.query.page, req.qeury.limit);
        return res.status(200).json({ status: 200, data: products });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }   
}