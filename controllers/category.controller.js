const CategoryService = require('../services/category.service');

exports.getCategories = async (req, res, next) => {
    try {
        const categories = await CategoryService.getCategories();
        return res.status(200).json({ status: 200, data: categories });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.createCategory = async (req, res, next) => {
    try {
        const category = await CategoryService.createCategory(req.body.title);
        return res.status(200).json({ status: 200, data: category });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeCategories = async (req, res, next) => {
    try {
        const resp = CategoryService.removeCategories();
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}


exports.getCategory = async (req, res, next) => {
    try {
        const category = await CategoryService.getCategory(req.params.categoryId);
        return res.status(200).json({ status: 200, data: category });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.updateCategory = async (req, res, next) => {
    try {
        await CategoryService.updateCategory(req.params.categoryId, req.body.title);
        const category = await CategoryService.getCategory(req.params.categoryId);
        return res.status(200).json({ status: 200, data: category });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeCategory = async (req, res, next) => {
    try {
        const resp = await CategoryService.removeCategory(req.params.categoryId);
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.addFilterToCategory = async (req, res, next)  => {
    try {
        await CategoryService.addFilterToCategory(req.params.categoryId, req.params.filterId);
        const category = CategoryService.getCategory(req.params.categoryId);
        return res.status(200).json({ status: 200, data: category });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeFilterFromCategory = async (req, res, next) => {
    try {
        await CategoryService.removeFilterFromCategory(req.params.categoryId, req.params.filterId);
        const category = CategoryService.getCategory(req.params.categoryId);
        return res.status(200).json({ status: 200, data: category });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}