var passport = require('passport');
const crypto = require('crypto');

const AuthService = require('../services/auth.service');
const AuthUtil = require('../utils/auth.util');
const EmailUtil = require('../utils/email.util');

const config = require('../config');

exports.getUser = async (req, res, next) => {
    try {
        const user = await AuthService.getUser(req.user._id);
        return res.status(200).json({ status: 200, data: user });
    }
    catch (e){
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
} 

exports.updateUser = async (req, res, next) => {
    try {
        await AuthService.updateUser(req.user._id, {...req.body, "admin": false});
        const user = await AuthService.getUser(req.user._id);
        return res.status(200).json({ status: 200, data: user });
    }
    catch(e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.loginUser = async (req, res, next) => {

    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err)
            return next(err);
        if (!user) {
            return res.status(401).json({success: false, status: 'Login Unsuccessful!', err: info});
        }
        req.logIn(user, (err) => {
            if (err) {
                return res.status(401).json({success: false, status: 'Login Unsuccessful!', err: err.message});          
            }
            EmailUtil.sendMail(req.user.email, 'login', { firstname: req.user.firstname, usernaame: req.user.ussername });

            var token = AuthUtil.getToken({_id: req.user._id});
            res.status(200).json({success: true, status: 'Login Successful!', token: token});
        });
    }) (req, res, next);
}

exports.registerUser = async (req, res, next) => {
    var user;
    try {
        user = await AuthService.findUser(req.body.username, req.body.email);
        if (user === null) {
            return res.status(406).json({ status: 406, message: 'User exists!'});
        }
        user = await AuthService.createUser(req.body.username, req.body.password, req.body.email, req.body.firstname, req.body.lastname);
        user = await AuthService.getUser(user._id);
        passport.authenticate('local')(req, res, () => res.status(200).json({ status: 200, message: 'Registration Successful'}));
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });       
    }
}

exports.checkUser = async (req, res, next) => {
    try {
        const user = await passport.authenticate('local', { session: false });
        if (!user) {
            return res.status(401).json({ status: 401});
        }
        return res.status(200).json({ status: 200 });
    }
    catch (e) {
        return res.status(401).json({ status: 401});
    }
}

exports.removeUser = async (req, res, next) => {
    try {
        const resp = await AuthService.removeUser(req.user._id);
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e){
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.createPasswordToken = async (req, res, next) => {
    try {
        const user = await AuthService.getUserByEmail(req.body.email);
        
        if (user === null) 
            throw Error(`User with email: ${req.body.email} does not exist`);
        const token = crypto.createHash('sha256').update(user._id + (new Date()).toString() + config.secretKey).digest('hex');
        await AuthService.updateUser(user._id, { "token": token });
        return res.status(200).json({ status: 200, data: token });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.resetPassword = async (req, res, next) => {
    try {
        var user = await AuthService.getUserByToken(req.params.tokenId);
        if (user === null) 
            throw Error(`Invalid Token`);
        await AuthService.resetPassword(user._id, req.body.password);
        user = await AuthService.getUser(user._id);
        return res.status(200).json({ status: 200, data: user });
    }   
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.updatePassword = async (req, res, next) => {
    try {
        await AuthService.updatePassword(req.user._id, req.body.old, req.body.new);
        user = await AuthService.getUser(req.user._id);
        return res.status(200).json({ status: 200, data: user });
    }   
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}