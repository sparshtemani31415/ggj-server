const UserService = require('../services/user.service');

exports.getCart = async (req, res, next) => {
    try {
        const cart = await UserService.getCart(req.user._id);
        return res.status(200).json({ status: 200, data: cart });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.updateCart = async (req, res, next) => {
    try {
        await UserService.updateCart(req.user._id, req.body);
        const cart = await UserService.getCart(req.user._id);
        return res.status(200).json({ status: 200, data: cart });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.getOrders = async (req, res, next) => {
    try {
        const orders = await UserService.getOrders(req.user._id);
        return res.status(200).json({ status: 200, data: orders });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.createOrder = async (req, res, next) => {
    try {
        const order = await UserService.createOrder(req.user._id, req.body);
        return res.status(200).json({ status: 200, data: order });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.getOrder = async (req, res, next) => {
    try {
        const order = await UserService.getOrder(req.params.orderId);
        return res.status(200).json({ status: 200, data: order });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.updateOrder = async (req, res, next) => {
    try {
        await UserService.updateOrder(req.params.orderId, req.body);
        const order = await UserService.getOrder(req.params.orderId);
        return res.status(200).json({ status: 200, data: order });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeOrder = async (req, res, next) => {
    try {
        const resp = await UserService.removeOrder(req.params.orderId);
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }   
}