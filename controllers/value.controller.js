const ValueService = require('../services/value.service');

exports.getValues = async (req, res, next) => {
    try {
        const values = await ValueService.getValues();
        return res.status(200).json({ status: 200, data: values });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.createValue = async (req, res, next) => {
    try {
        const value = await ValueService.createValue(req.body.title);
        return res.status(200).json({ status: 200, data: value });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeValues = async (req, res, next) => {
    try {
        const resp = await ValueService.removeValues();
        return res.status(200).json({ status: 200, data: resp });
    }
    catch(e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.getValue = async (req, res, next) => {
    try {
        const value = await ValueService.getValue(req.params.valueId);
        return res.status(200).json({ status: 200, data: value });
    }
    catch(e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.updateValue = async (req, res, next) => {
    try {
        await ValueService.updateValue(req.body.title);
        const value = await ValueService.getValue(req.params.valueId);
        return res.status(200).json({ status: 200, data: value });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.removeValue = async (req, res, next) => {
    try {
        const resp = await ValueService.removeValue(req.params.valueId);
        return res.status(200).json({ status: 200, data: resp });
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}