const UserService = require('../services/user.service');

const PaymentUtil = require('../utils/payment.util');

exports.createPayment = async (req, res, next) => {
    try {

        // const payment = PaymentUtil.createForm('0005', '1234', '7815917606', 'sparshtemaniii@gmail.com', '1');

        // res.writeHead(200, { 'Content-Type': 'text/html'});
        // res.write(payment.form);
        // res.end();

        const order = await UserService.getOrder(req.params.orderId);
        if (order.status === 0) {

            const payment = PaymentUtil.createForm(order._id, req.user._id, order.phno, req.user.email, order.total);

            await UserService.updateOrder(req.params._id, { "checksum": payment.checksum });

            res.writeHead(200, { 'Content-Type': 'text/html'});
            res.write(payment.form);
            res.end();

        }
        throw Error('Payment has been completed before!');

    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}

exports.confirmPayment = async (req, res, next) => {
    try {

        const order = UserService.getOrder(req.params.orderId);
        if (order.checksum === req.body['CHECKSUMHASH']) {
            await UserService.updateOrder(req.params._id, { "txn": req.body });
            res.redirect(307, `https://${req.hostname}:${app.get('secPort')}/order/${order._id}`);
        }
        else {
            throw Error('Payment could not be verified!');
        }
    }
    catch (e) {
        return res.status(e.status || 406).json({ status: e.status || 406, message: e.message });
    }
}