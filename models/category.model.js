const mongoose = require('mongoose');

const Product = require('./product.model');

const Category = new mongoose.Schema({

    title: {
        type:String,
        required: true,
        unique: true,
        dropDups: true
    },
    
    filters: [{ 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Filter'
    }],

});

Category.post('remove', { query: true, document: false }, async (query) => {
    await Product.updateMany({}, { $set: { "category": null } });
});

Category.post('remove', { query: false, document: true }, async (category) => {
    await Product.updateMany({ "category": caregory._id }, { $set: { "category": null } });
});

module.exports = mongoose.model('Category', Category);