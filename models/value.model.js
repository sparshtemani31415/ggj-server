const mongoose = require('mongoose');

const Filter = require('./filter.model');
const Product = require('./product.model');

const Value = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true,
        dropDups: true  
    },
});

Value.post('remove', { query: true, document: false }, async (query) => {
    await Filter.updateMany({}, { $set: { "values": [] } });
    await Product.updateMany({}, { $set: { "values": [] } });
});

Value.post('remove', { query: false, document: true }, async (value) => {
    await Filter.updateMany({ "values": { $elemMatch: { $eq: value._id } } }, { $pull: { "values": value._id } });
    await Product.updateMany({ "values": { $elemMatch: { $eq: value._id } } }, { $pull: { "values": value._id } });
});

module.exports = mongoose.model('Value', Value);