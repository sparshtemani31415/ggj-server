const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

var CartItem = new mongoose.Schema({
    item: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },
    quantity: {
        type: Number,
        default: 1
    }
});

var User = new mongoose.Schema({
    
    firstname: {
        type: String,
        default: '',
    },
    lastname: {
        type: String,
        default: '',
    },
    admin: {
        type: Boolean,
        default: false,
    },
    email: {
        type: String,
        default: '',
        unique: true,
        dropDups: true,
        required: true
    },
    token: {
        type: String,
        default: ''
    },
    cart: [CartItem],
    order: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Order'
    }],
});

User.plugin(passportLocalMongoose);
module.exports = mongoose.model('User', User);