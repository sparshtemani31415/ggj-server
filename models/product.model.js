const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate');

const FileUtil = require('../utils/file.util');

const Product = new mongoose.Schema({

    title: {
        type: String,
        required: true,
    },

    sku: {
        type: String,
        required: true,
        unique: true,
        dropDups: true
    },

    description: {
        type: String,
        required: true,
    },

    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },

    price: {
        type: Number,
        required: true,
    },

    quantity: {
        type: Number,
        required: true,
        default: 1
    },

    mainImage: {
        type: String,
        required: true
    },

    images: [{
        type: String
    }],

    values: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Value'
    }],

});

Product.post('remove', { query: true, document: false }, async (query) => {
    await FileUtil.removeFile(this.mainImage);
    this.images.forEach(async (image) => {
        await FileUtil.removeFile(image);
    });
});

Product.plugin(mongoosePaginate);
module.exports = mongoose.model('Product', Product);