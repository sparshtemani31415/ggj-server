const mongoose = require('mongoose');

var OrderItem = new mongoose.Schema({

    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },

    quantity: {
        type: Number,
        required: true
    },

    price: {
        type: Number,
        required: true
    }

});

var Order = new mongoose.Schema({

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

    products: [OrderItem],

    total: {
        type: Number,
        required: true
    },

    phno: { 
        type: String,
        required: true
    },

    address: {
        type: String,
        required: true
    },

    status: {
        type: Number,
        default: 0
    },

    checksum: {
        type: String,
        default: '',
        unique: true,
        dropDups: true
    },

    txn: {
        type: Object,
        default: {},
        unique: true,
        dropDups: true
    }

}, {
    timestamp: true
});

module.exports = mongoose.model('Order', Order);