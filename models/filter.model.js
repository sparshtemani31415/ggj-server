const mongoose = require('mongoose');

const Category = require('./category.model');

const Filter = new mongoose.Schema({

    title: {
        type: String,
        required: true,
        unique: true,
        dropDups: true  
    },

    values: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Value'
    }]

});

Filter.post('remove', { query: true, document: false }, async (query) => {
    await Category.updateMany({}, { $set: { "filters": [] } });
});

Filter.post('remove', { query: false, document: true }, async (filter) => {
    await Category.updateMany({ "filters": { $elemMatch: { $eq: filter._id } } }, { $pull: { "filters": filter._id } });
});

module.exports = mongoose.model('Filter', Filter);