const express = require('express');
const mongoose = require('mongoose');

var passport = require('passport');
var morgan = require('morgan')
var config = require('./config');

const AuthRouter = require('./routes/auth.route');
const ValueRouter = require('./routes/value.route');
const FilterRouter = require('./routes/filter.route');
const CategoryRouter = require('./routes/category.route');
const ProductRouter = require('./routes/product.route');
const PaymentRouter = require('./routes/payment.route');

var app = express();

// Redirect to Secure port
app.all('*', (req, res, next) => {
    if (req.secure) return next();
    else res.redirect(307, `https://${req.hostname}:${app.get('secPort')}${req.url}`);
});

// Preprocessor - Middleware
app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(passport.initialize());

// Database Connections
const URL = config.mongoUrl;

mongoose.set('useCreateIndex', true);
mongoose.connect(URL, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.Promise = global.Promise;
mongoose.connection.on('error', console.error.bind(console, 'MongoDB Connection Error...'));

// Routes
app.use('/api/auth', AuthRouter);
app.use('/api/value', ValueRouter);
app.use('/api/filter', FilterRouter);
app.use('/api/category', CategoryRouter);
app.use('/api/product', ProductRouter);
app.use('/api/payment', PaymentRouter);

app.use(function(err, req, res, next) {  
    res.status(err.status || 500);
    res.end(err.message);
});

module.exports = app;