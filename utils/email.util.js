const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');

const config = require('../config');

var mailer = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
        user: config.email,
        pass: config.password
    }
});

mailer.use('compile', hbs({
    viewEngine: {
        extName: '.hbs',
        partialsDir: 'views/templates/email',
        layoutsDir: 'views/layouts',
        defaultLayout: 'email.hbs'
    },
    viewPath: 'views/email',
    extName: '.hbs'
}));

exports.sendMail = (toMail, template = 'login.email', context = {}) => {
    mailer.sendMail({
        from: config.email,
        to: toMail,
        subject: 'New Login at GGJ!',
        template: template,
        context: context 
    }, (err, res) => {
        if (err) {
            throw err;
        }
    });
}