const fs = require('fs');
const path = require('path');

exports.removeFile = async (fileName, filePath = 'public/media/products') => {
    try {
        fs.unlinkSync(path.join(filePath, fileName));
    }
    catch (e) {
        throw e;
    }
}