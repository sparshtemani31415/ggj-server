var checksum_lib = require('./checksum/checksum');
var config = require('../config');

var checksum;

exports.createForm = (ORDERID, CUSTID, MOBILENO, EMAIL, TXN_AMOUNT) => {

    var params = {
        "MID": config.MID,
        "WEBSITE": "WEBSTAGING",
        "INDUSTRY_TYPE_ID": "Retail",
        "CHANNEL_ID": "WEB",
        "ORDER_ID": ORDERID,
        "CUST_ID": CUSTID,
        "MOBILE_NO": MOBILENO,
        "EMAIL": EMAIL,
        "TXN_AMOUNT": TXN_AMOUNT,
        "CALLBACK_URL": `${config.baseURL}:${config.PORT}/api/payment/${ORDERID}`,
    };

    checksum_lib.genchecksum(params, config.MKEY, (err, checksum) => {
        global.checksum = checksum;
    });

    let checksum = global.checksum;

    console.log(checksum);

    var form = '';

    var url = config.URL;

    form += '<html><head><title>Merchant Checkout Page</title></head><body><center><h1>Please do not refresh this page...</h1></center><form method="post" action="' + url + '" name="paytm_form">';

    for(var x in params){
        form += '<input type="hidden" name="' + x + '" value="' + params[x] + '">';
    }

    form += '<input type="hidden" name="CHECKSUMHASH" value="' + checksum + '"></form><script type="text/javascript">document.paytm_form.submit();</script></body></html>';

    return {
        form: form,
        checksum: checksum
    };
    
}