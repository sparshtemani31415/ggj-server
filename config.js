module.exports =  {
    'secretKey': "<Your secret here>",
    'mongoUrl': "<Mongo URL>",
    'PORT': "<PORT NUMBER>",
    'baseURL': "https://localhost",
    "MID": "<Paytm Merchant Id>",
    "MKEY": "<Paytm Merchant Key>",
    "URL": "<Paytm Redirect URL>",
    "email": "<Gmail Account login>",
    "password": "<Gmail account password>"
};
