const Filter = require('../models/filter.model');

exports.verifyFilter = async (req, res, next) => {
    try {
        const filter = await Filter.findById(req.params.filterId);
        if (filter == null)
            throw Error(`${req.params.filterId} is Invalid!`);
        next();
    }
    catch (e) {
        return res.status(406).json({ status: 406, message: e.message });
    }
}