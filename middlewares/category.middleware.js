const Category = require('../models/category.model');

exports.verifyCategory = async (req, res, next) => {
    try {
        const category = await Category.findById(req.params.categoryId);
        if (category == null)
            throw Error(`${req.params.categoryId} is Invalid!`);
        next();
    }
    catch (e) {
        return res.status(406).json({ status: 406, message: e.message });
    }
}