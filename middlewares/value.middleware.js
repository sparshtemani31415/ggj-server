const Value = require('../models/value.model');

exports.verifyValue = async (req, res, next) => {
    try {
        const value = await Value.findById(req.params.valueId);
        if (value == null)
            throw Error(`${req.params.valueId} is Invalid!`);
        next();
    }
    catch (e) {
        return res.status(406).json({ status: 406, message: e.message });
    }
}