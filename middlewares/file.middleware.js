var multer = require('multer');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/media/products');
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + '_' + file.originalname);
    }
});

exports.upload = multer({ storage: storage });