const Product = require('../models/product.model');

exports.verifyProduct = async (req, res, next) => {
    try {
        const product = await Product.findById(req.params.productId);
        if (product == null)
            throw Error(`${req.params.productId} is Invalid!`);
        next();
    }
    catch (e) {
        return res.status(406).json({ status: 406, message: e.message });
    }
}