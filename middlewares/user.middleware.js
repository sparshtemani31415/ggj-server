const Order = require('../models/order.model');

exports.verifyOrder = async (req, res, next) => {
    try {
        const order = await Order.findById(req.params.orderId);
        if (order)
            throw Error(`${req.params.orderId} is Invalid!`);
        if (order.user !== req.user._id && req.user.admin === false)
            throw Error('Unauthorized');
        next();
    }
    catch (e) {
        return res.status(406).json({ status: 406, message: e.message });
    }
}