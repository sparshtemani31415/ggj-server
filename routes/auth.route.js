var express = require('express');
const bodyParser = require('body-parser');

var router = express.Router();
router.use(bodyParser.json());

const AuthMiddleware = require('../middlewares/auth.middleware');
const ErrorMiddleware = require('../middlewares/error.middleware');

const AuthController = require('../controllers/auth.controller');

router.route('/')
.get(AuthMiddleware.verifyUser, AuthController.getUser)
.post(ErrorMiddleware.notSupported)
.put(AuthMiddleware.verifyUser, AuthController.updateUser)
.delete(AuthMiddleware.verifyUser, AuthController.removeUser);

router.route('/register')
.get(ErrorMiddleware.notSupported)
.post(AuthController.registerUser)
.put(ErrorMiddleware.notSupported)
.delete(ErrorMiddleware.notSupported);

router.route('/login')
.get(AuthController.loginUser)
.post(ErrorMiddleware.notSupported)
.put(ErrorMiddleware.notSupported)
.delete(ErrorMiddleware.notSupported);

router.route('/check')
.get(AuthMiddleware.verifyUser, AuthController.checkUser)
.post(ErrorMiddleware.notSupported)
.put(ErrorMiddleware.notSupported)
.delete(ErrorMiddleware.notSupported);

router.route('/forgot')
.get(ErrorMiddleware.notSupported)
.post(AuthController.createPasswordToken)
.put(AuthMiddleware.verifyUser, AuthController.updatePassword)
.delete(ErrorMiddleware.notSupported);

router.route('/reset/:tokenId')
.get(ErrorMiddleware.notSupported)
.post(ErrorMiddleware.notSupported)
.put(AuthController.resetPassword)
.delete(ErrorMiddleware.notSupported);

module.exports = router;