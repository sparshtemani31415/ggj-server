var express = require('express');
const bodyParser = require('body-parser');

var router = express.Router();
router.use(bodyParser.json());

const AuthMiddleware = require('../middlewares/auth.middleware');
const ErrorMiddleware = require('../middlewares/error.middleware');
const ValueMiddleware = require('../middlewares/value.middleware');
const FilterMiddleware = require('../middlewares/filter.middleware');

const FilterController = require('../controllers/filter.controller');

router.route('/')
.get(FilterController.getFilters)
.post(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, FilterController.createFilter)
.put(ErrorMiddleware.notSupported)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, FilterController.removeFilters);

router.route('/:filterId')
.get(FilterMiddleware.verifyFilter, FilterController.getFilter)
.post(ErrorMiddleware.notSupported)
.put(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, FilterMiddleware.verifyFilter, FilterController.updateFilter)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, FilterMiddleware.verifyFilter, FilterController.removeFilter);

router.route('/:filterId/:valueId')
.get(ErrorMiddleware.notSupported)
.post(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ValueMiddleware.verifyValue, FilterMiddleware.verifyFilter, FilterController.addValueToFilter)
.put(ErrorMiddleware.notSupported)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ValueMiddleware.verifyValue, FilterMiddleware.verifyFilter, FilterController.removeValueFromFilter);

module.exports = router;