var express = require('express');
const bodyParser = require('body-parser');

var router = express.Router();
router.use(bodyParser.json());

const AuthMiddleware = require('../middlewares/auth.middleware');
const ErrorMiddleware = require('../middlewares/error.middleware');
const FilterMiddleware = require('../middlewares/filter.middleware');
const CategoryMiddleware = require('../middlewares/category.middleware');

const CategoryController = require('../controllers/category.controller');

router.route('/')
.get(CategoryController.getCategories)
.post(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, CategoryController.createCategory)
.put(ErrorMiddleware.notSupported)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, CategoryController.removeCategories);

router.route('/:categoryId')
.get(CategoryMiddleware.verifyCategory, CategoryController.getCategory)
.post(ErrorMiddleware.notSupported)
.put(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, CategoryMiddleware.verifyCategory, CategoryController.updateCategory)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, CategoryMiddleware.verifyCategory, CategoryController.removeCategory);

router.route('/:categoryId/:filterId')
.get(ErrorMiddleware.notSupported)
.post(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, FilterMiddleware.verifyFilter, CategoryMiddleware.verifyCategory, CategoryController.addFilterToCategory)
.put(ErrorMiddleware.notSupported)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, FilterMiddleware.verifyFilter, CategoryMiddleware.verifyCategory, CategoryController.removeFilterFromCategory);

module.exports = router;