var express = require('express');
const bodyParser = require('body-parser');

var router = express.Router();
router.use(bodyParser.json());

const AuthMiddleware = require('../middlewares/auth.middleware');
const ErrorMiddleware = require('../middlewares/error.middleware');
const ValueMiddleware = require('../middlewares/value.middleware');

const ValueController = require('../controllers/value.controller');

router.route('/')
.get(ValueController.getValues)
.post(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ValueController.createValue)
.put(ErrorMiddleware.notSupported)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ValueController.removeValues);

router.route('/:valueId')
.get(ValueMiddleware.verifyValue, ValueController.getValue)
.post(ErrorMiddleware.notSupported)
.put(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ValueMiddleware.verifyValue, ValueController.updateValue)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ValueMiddleware.verifyValue, ValueController.removeValue);

module.exports = router;