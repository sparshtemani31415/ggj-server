var express = require('express');
const bodyParser = require('body-parser');

var router = express.Router();
router.use(bodyParser.json());

const AuthMiddleware = require('../middlewares/auth.middleware');
const ErrorMiddleware = require('../middlewares/error.middleware');
const FileMiddleware = require('../middlewares/file.middleware');
const ValueMiddleware = require('../middlewares/value.middleware');
const CategoryMiddleware = require('../middlewares/category.middleware');
const ProductMiddleware = require('../middlewares/product.middleware');

const ProductController = require('../controllers/product.controller');

router.route('/')
.get(ProductController.getProducts)
.post(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, FileMiddleware.upload.single('img'), ProductController.createProduct)
.put(ErrorMiddleware.notSupported)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ProductController.removeProducts);

router.route('/image/:productId')
.get(ErrorMiddleware.notSupported)
.post(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ProductMiddleware.verifyProduct, FileMiddleware.upload.single('img'), ProductController.addImage)
.put(ErrorMiddleware.notSupported)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ProductMiddleware.verifyProduct, ProductController.removeImage);

router.route('/:productId')
.get(ProductMiddleware.verifyProduct, ProductController.getProduct)
.post(ErrorMiddleware.notSupported)
.put(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ProductMiddleware.verifyProduct, ProductController.updateProduct)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ProductMiddleware.verifyProduct, ProductController.removeProduct);

router.route('/:productId/:valueId')
.get(ErrorMiddleware.notSupported)
.post(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ValueMiddleware.verifyValue, ProductMiddleware.verifyProduct, ProductController.addValueToProduct)
.put(ErrorMiddleware.notSupported)
.delete(AuthMiddleware.verifyUser, AuthMiddleware.verifyAdmin, ValueMiddleware.verifyValue, ProductMiddleware.verifyProduct, ProductController.removeValueFromProduct);

router.route('/category/:categoryId')
.get(CategoryMiddleware.verifyCategory, ProductMiddleware.verifyProduct, ProductController.getProductsWithCategory)
.post(ErrorMiddleware.notSupported)
.put(ErrorMiddleware.notSupported)
.delete(ErrorMiddleware.notSupported);

module.exports = router;