var express = require('express');
const bodyParser = require('body-parser');

const AuthMiddleware = require('../middlewares/auth.middleware');
const ErrorMiddleware = require('../middlewares/error.middleware');
const UserMiddleware = require('../middlewares/user.middleware');

const PaymentController = require('../controllers/payment.controller');

var router = express.Router();
router.use(bodyParser.json());

router.route('/:orderId')
.get(PaymentController.createPayment)
.post(PaymentController.confirmPayment)
.put(ErrorMiddleware.notSupported)
.delete(ErrorMiddleware.notSupported);

module.exports = router;