var express = require('express');
const bodyParser = require('body-parser');

var router = express.Router();
router.use(bodyParser.json());

const AuthMiddleware = require('../middlewares/auth.middleware');
const ErrorMiddleware = require('../middlewares/error.middleware');
const UserMiddleware = require('../middlewares/user.middleware');

const UserController = require('../controllers/user.controller');

router.route('/cart')
.get(AuthMiddleware.verifyUser, UserController.getCart)
.post(ErrorMiddleware.notSupported)
.put(AuthMiddleware.verifyUser, UserController.updateCart)
.delete(ErrorMiddleware.notSupported);

router.route('/order')
.get(AuthMiddleware.verifyUser, UserController.getOrders)
.post(AuthMiddleware.verifyUser, UserController.createOrder)
.put(ErrorMiddleware.notSupported)
.delete(ErrorMiddleware.notSupported);

router.route('/order/:orderId')
.get(UserMiddleware.verifyOrder, AuthMiddleware.verifyUser, UserController.getOrder)
.post(ErrorMiddleware.notSupported)
.put(AuthMiddleware.verifyAdmin, AuthMiddleware.verifyUser, UserMiddleware.verifyOrder, UserController.updateOrder)
.delete(AuthMiddleware.verifyAdmin, AuthMiddleware.verifyUser, UserMiddleware.verifyOrder, UserController.deleteOrder);

module.exports = router;