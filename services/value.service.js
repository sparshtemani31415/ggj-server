const Value = require('../models/value.model');

exports.getValues = async () => {
    try {
        const values = await Value.find({});
        return values;
    }
    catch (e) {
        throw e;
    }
}

exports.createValue = async (title) => {
    try {
        value = await Value.create({ "title" : title });
        return value;
    }
    catch (e) {
        throw e;
    }
}

exports.removeValues = async () => {
    try {
        const resp = await Value.remove({});
        return resp;
    }
    catch (e) {
        throw e;
    }
}

exports.getValue = async (valueId) => {
    try {
        const value = await Value.findById(valueId);
        return value;
    }
    catch (e) {
        throw e;
    }
}

exports.updateValue = async (valueId, title) => {
    try {
        await Value.findByIdAndUpdate(valueId, { $set: { "title": title } });
    }
    catch (e) {
        throw e;
    }
}

exports.removeValue = async (valueId) => {
    try {
        const resp = await Value.remove({ _id: valueId });
        return resp;
    }
    catch (e) {
        throw e;
    }
}