const User = require('../models/user.model');
const Order = require('../models/order.model');

exports.getCart = async (userId) => {
    try {
        const cart = await User.findById(userId)
        .populate('cart')
        .populate({ path: "cart.item", model: 'Product' }).cart;
        return cart;
    }
    catch (e) {
        throw e;
    }
}

exports.updateCart = async (userId, updates) => {
    try {
        await User.findByIdAndUpdate(userId, updates);
    }
    catch (e) {
        throw e;
    }
}

exports.getOrders = async (userId) => {
    try {
        const orders = await User.findById(userId)
        .populate('orders')
        .populate('orders.products')
        .populate({ path: "orders.products.product", model: 'Product' }).order;
        return orders;
    }
    catch (e) {
        throw e;
    }
}

exports.createOrder = async (userId, details) => {
    try {
        const order = await Order.create({ ...details, user: userId });
        return order;
    }
    catch (e) {
        throw e;
    }
}

exports.getOrder = async (orderId) => {
    try {
        const order = await Order.findById(orderId)
        .populate('products')
        .populate({ path: "products.product", model: 'Product' }).order;
        return order;
    }
    catch (e) {
        throw e;
    }
}

exports.updateOrder = async (orderId, updates) => {
    try {
        await Order.findByIdAndUpdate(orderId, { $set: updates });
    }
    catch (e) {
        throw e;
    }
}

exports.removeOrder = async (orderId) => {
    try {
        const resp = await Order.remove({ _id: orderId });
        return resp;
    }
    catch (e) {
        throw e;
    }
}