const Filter = require('../models/filter.model');

exports.getFilters = async () => {
    try {
        const filters = await Filter.find({})
        .populate('values');
        return filters;
    }
    catch (e) {
        throw e;
    }
}

exports.createFilter = async (title) => {
    try {
        const filter = await Filter.create({ "title": title });
        return filter;
    }
    catch (e) {
        throw e;
    }
}

exports.removeFilters = async () => {
    try {
        const resp = await Filter.remove({});
        return resp;
    }
    catch (e) {
        throw e;
    }
}

exports.getFilter = async (filterId) => {
    try {
        const filter = await Filter.findById(filterId)
        .populate('values');
        return filter;
    }
    catch (e) {
        throw e;
    }
}

exports.updateFilter = async (filterId, title) => {
    try {
        await Filter.findByIdAndUpdate(filterId, { $set: { "title": title } });
    }
    catch (e) {
        throw e;
    }
}

exports.removeFilter = async (filterId) => {
    try {
        const resp = Filter.remove({ _id: filterId });
        return resp;
    }
    catch (e) {
        throw e;
    }
}

exports.addValueToFilter = async (filterId, valueId) => {
    try {
        await Filter.findByIdAndUpdate(filterId, { $addToSet: { "values": valueId } });
    }
    catch (e) {
        throw e;
    }
}

exports.removeValueFromFilter = async (filterId, valueId) => {
    try {
        await Filter.findByIdAndUpdate(filterId, { $pull: { "values": valueId } });
    }
    catch (e) {
        throw e;
    }
}