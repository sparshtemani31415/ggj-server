const Product = require('../models/product.model');
const FileUtil = require('../utils/file.util');

exports.getProducts = async (page, limit) => {
    try {
        const products = await Product.paginate({}, { page: page, limit: limit })
        .populate('values');
        return products;
    }
    catch (e) {
        throw e;
    }
}

exports.createProduct = async (details, imageName) => {
    try {
        const product = await Product.create({ ...details, "mainImage": imageName });
        return product;
    }
    catch (e) {
        throw e;
    }
}

exports.removeProducts = async () => {
    try {
        const resp = await Product.remove({});
        return resp;
    }
    catch (e) {
        throw e;
    }
}

exports.updateMainImage = async (productId, imageName) => {
    try {
        const product = await Product.findById(productId);
        await Product.findByIdAndUpdate(productId, { $set: { "mainImage": imageName } });
        await FileUtil.removeFile(product.mainImage);
    }
    catch (e) {
        throw e;
    }
}

exports.addImage = async (productId, imageName) => {
    try {
        await Product.findByIdAndUpdate(productId, { $addToSet: { "images": imageName } });
    }
    catch (e) {
        throw e;
    }
}

exports.deleteImage = async (productId, imageName) => {
    try {
        await Product.findByIdAndUpdate(productId, { $pull: { "images": imageName } });
        await FileUtil.removeFile(imageName);
    }
    catch (e) {
        throw e;
    }
}

exports.getProduct = async (productId) => {
    try {
        const product = await Product.findById(productId)
        .populate('values');
        return product;
    }
    catch (e) {
        throw e;
    }
}

exports.updateProduct = async (productId, updates) => {
    try {
        await Product.findByIdAndUpdate(productId, { $set: updates } );
    }
    catch (e) {
        throw e;
    }
}

exports.removeProduct = async (productId) => {
    try {
        const resp = Product.remove({ _id: productId });
        return resp;
    }
    catch (e) {
        throw e;
    }
}

exports.addValuetoProduct = async (productId, valueId) => {
    try {
        await Product.findByIdAndUpdate(productId, { $addToSet: { "values": valueId } });
    }
    catch (e) {
        throw e;
    }
}

exports.removeValueFromProduct = async (productId, valueId) => {
    try {
        await Product.findByIdAndUpdate(productId, { $pull: { "values": valueId } });
    }
    catch (e) {
        throw e;
    }
}

exports.getProductsWithCategory = async (categoryId, page, limit) => {
    try {
        const products = await Product.paginate({ "category": categoryId }, { page: page, limit: limit })
        .populate('values');
        return products;
    }
    catch (e) {
        throw e;
    }
}