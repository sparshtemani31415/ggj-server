const User = require('../models/user.model');

exports.getUser = async (userId) => {
    try {
        const user = await User.findById(userId);
        return user;
    }
    catch (e) {
        throw e;
    }
}

exports.createUser = async (username, password, email, firstname, lastname) => {
    try {
        var user = await User.register(new User({username: username, email: email}), password);
        user.firstname = firstname;
        user.lastname = lastname;
        user = await user.save();
        return user;
    }
    catch (e) {
        throw e;
    }
}

exports.updateUser = async (userId, updates) => {
    try {
        await User.findByIdAndUpdate(userId, { $set: updates });
    }
    catch (e) {
        throw e;
    }
}

exports.findUser = async (username, email) => {
    try {
        const user = await User.findOne({ $or: [ { "username": username }, { "email": email } ] });
        return user === null ? true : false;
    }
    catch (e) {
        throw e;
    }
}

exports.removeUser = async (userId) => {
    try {
        const resp = await User.findByIdAndDelete(userId);
        return resp;
    }
    catch (e) {
        throw e;
    }
}

exports.getUserByEmail = async (email) => {
    try {
        const user = await User.findOne({ "email": email });
        return user;
    }
    catch (e) {
        throw e;
    }
}

exports.getUserByToken = async (token) => {
    try {
        const user = await User.findOne({ "token": token });
        return user;
    }
    catch (e) {
        throw e;
    }
}

exports.resetPassword = async (userId, password) => {
    try {
        var user = await User.findById(userId);
        user.setPassword(password, async (err, user) => {
            if (err) throw err;
            user.token = '';
            await user.save();
        });
    }
    catch (e) {
        throw e;
    }
}

exports.updatePassword = async (userId, oldPassword, newPassword) => {
    try {
        var user = await User.findById(userId);
        user.changePassword(oldPassword, newPassword, (err, user) => {
            if (err) throw err;
        });
    }
    catch (e) {
        throw e;
    }
}