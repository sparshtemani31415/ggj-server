const Category = require('../models/category.model');

exports.getCategories = async () => {
    try {
        const categories = await Category.find({})
        .populate('filters');
        return categories;
    }
    catch (e) {
        throw e;
    }
}

exports.createCategory = async (title) => {
    try {
        const category = await Category.create({ "title": title });
        return category;
    }
    catch (e) {
        throw e;
    }
}

exports.removeCategories = async () => {
    try {
        const resp = await Category.remove({});
        return resp;
    }
    catch (e) {
        throw e;
    }
}

exports.getCategory = async (categoryId) => {
    try {
        const category = await Category.findById(categoryId)
        .populate('filters');
        return category;
    }
    catch (e) {
        throw e;
    }
}

exports.updateCategory = async (categoryId, title) => {
    try {
        await Category.findByIdAndUpdate(categoryId, { $set: { "title": title } });
    }
    catch (e) {
        throw e;
    }
}

exports.removeCategory = async (categoryId) => {
    try {
        const resp = Category.remove({ _id: categoryId });
        return resp;
    }
    catch (e) {
        throw e;
    }
}

exports.addFilterToCategory = async (categoryId, filterId) => {
    try {
        await Category.findByIdAndUpdate(categoryId, { $addToSet: { "filters": filterId } });
    }
    catch (e) {
        throw e;
    }
}

exports.removeFilterFromCategory = async (categoryId, filterId) => {
    try {
        await Category.findByIdAndUpdate(categoryId, { $pull: { "filters": filterId } });
    }
    catch (e) {
        throw e;
    }
}